#include <iostream>
using namespace std;

int main()
{
    int numero; //aqui guardaremos el numero que ser� ingresado por el usuario
    cout << "Ingrese un numero N, para calcular los primeros N numeros primos" << endl;
    cin >> numero;
    int contadorDeDivisores = 0;
    cout << "Su numero ingresado es: " << numero <<endl;
    if (numero == 0 || numero == 1 || numero < 0) return false;
    int primo;
    for (int i = 2; i <= numero; i++) { 
        for (int j = 1; j <= i; j++) {
            if (i % j == 0) {
                contadorDeDivisores++;
            }
        }
        if (contadorDeDivisores == 2) {
            cout << "El numero " << i << " es primo" <<endl;
        }
        contadorDeDivisores = 0;
    }
}

