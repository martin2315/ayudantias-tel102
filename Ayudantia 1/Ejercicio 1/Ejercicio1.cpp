
#include <iostream> //le dice al compilador que incluya las declaraciones I/O de la biblioteca estandar
using namespace std;  //le dice al compilador que la funcion cout es de la libreria estandar.
int main()
{
    int numero; //variable donde guardaremos el numero ingresado por el usuario
    int resultado = 1;
    cout << "Bienvenido al ejercicio 1"<<endl;
    cout << "Ingrese un numero para calcular su factorial"<<endl;
    cin >> numero; //El numero ingresado por el usuario se almacenar� en la variable numero;
    cout <<numero << endl;


    //Opcion factorial ocupando ciclo while
   /* while (numero > 0) {
        resultado = numero * resultado;
        numero--;

    }*/

    //Opcion factorial ocupando ciclo For
    for (int i = 1; i <=numero; i++) {
        resultado = resultado*i;
    }
    cout << "El factorial de su numero es: "<< resultado << endl;
    return 0;
}
