#pragma once
/*--------Prototipos de funcion ejercicio 1.1 --------------------*/


//funcion para calcula la aproximacion del seno
double senoAprox(double x, int n);

//funcion para calcular el factorial de un numero "i"
double factorial(int i);

//funcion para elevar un numero
double elevarNumero(double numero, int cantidadDeVeces);

/*-----------Prototipos de funcion ejercicio 1.2----------------*/

//funcion para determinar el numero N de la aproximacion ;
int determinarNprecision(double x, double p);

//funcion para determinar el valor absoluto de la resta entre 2 numeros.
double valorAbsoluto(double numero1, double numero2);