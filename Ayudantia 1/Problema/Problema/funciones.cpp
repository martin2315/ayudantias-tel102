#include "funciones.h"
#include<iostream>
using namespace std;
/*------------Implementacion de funciones ejercicio 1.1---------------------------*/

//implementacion de funcion que nos calcular� el factorial de un numero i
double factorial(int i) {
    if (i <= 1) return 1;
    else return i * factorial(i - 1);
}

//implementacion de una funcion que eleva un numero en "cantidadDeVeces" ej: numero^(cantidadDeVeces)
double elevarNumero(double numero, int cantidadDeVeces) {
    double resultado = 1;
    for (int i = 0; i < cantidadDeVeces; i++) {
        resultado = resultado * numero;
    }
    return resultado;
}

//implementacion de la funcion que aproxima un seno
double senoAprox(double x, int n) {
    double aproximacion = 0;
    for (int i = 0; i <= n; i++) {
        aproximacion = aproximacion + (elevarNumero(-1, i) / factorial(2 * i + 1)) * elevarNumero(x, 2 * i + 1);
    }
    return aproximacion;
}


/*-----------Implementacion de funciones ejercicio 1.2---------------------*/
double valorAbsoluto(double numero1, double numero2) {
    if (numero1 - numero2 < 0) {
        return ((numero1 - numero2) * -1);
    }
    else {
        return (numero1 - numero2);
    }
}

int determinarNprecision(double x, double p) {
    double resultado = 0;
    int n = 0;
    while (true) {
        resultado = valorAbsoluto(senoAprox(x, n), sin(x));
        if (resultado < p) {
            break;
        }
        else {
            n++;
        }
    }
    cout << "resultado: " << senoAprox(x, n) << endl;
    return n;
}