#include "funciones.h"
#include<cmath>
#include <iostream>
using namespace std;

//funcion main donde de corrobora el resultado
int main() {

    //corroborando ejercicio 1
    cout << "-----------------corroborando ejercicio 1----------------" << endl;
    double resultado = senoAprox(3.14, 7);
    cout <<"El resultado de la aproximacion es: " << resultado << endl;
    cout << "-------------------------------" << endl << endl;


    //corroborando ejercicio 2
    cout << "-----------------corroborando ejercicio 2----------------" << endl;
    cout << "El numero n es: "<< determinarNprecision(1.42, 0.001) << endl;
    return 0;
}
