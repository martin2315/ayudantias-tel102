#pragma once

#include"Beca.h"
class BecaNovato : public Beca
{
public:
	BecaNovato(int id, int s, int tipo) : Beca(id, s, tipo) {};
	bool depositar(int d);
	bool retirar(int d);
};

bool BecaNovato::depositar(int d) {
	if ((saldo + d) <= saldoMax_bnovato && d <= retiroMax_bnovato) {
		this->saldo += d;
		return true;
	}
	else {
		return false;
	}

}

bool BecaNovato::retirar(int d) {
	if (this->saldo - d >= 0) {
		this->saldo -= d;
		return true;
	}
	else {
		return false;
	}
}
