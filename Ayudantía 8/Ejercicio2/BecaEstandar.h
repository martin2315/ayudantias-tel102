#pragma once

#include"Beca.h"
class BecaEstandar : public Beca
{
public:
	BecaEstandar(int id, int s, int tipo) : Beca(id, s, tipo) {};
private:

	bool depositar(int d);
	bool retirar(int d);
};

bool BecaEstandar::depositar(int d) {
	this->saldo += d;
	return true;
}

bool BecaEstandar::retirar(int d) {
	if (this->saldo - d > -5000) {
		this->saldo -= d;
		return true;
	}
	else {
		return false;
	}
}