

#include <iostream>
#include "Beca.h"
#include "Universidad.h"
#include "BecaEstandar.h"
#include "BecaNovato.h"
using namespace std;
int main()
{
    //Creando 2 instancias de la Clase BecaNovato
    BecaNovato beca1(1124, 40000, 1);
    BecaNovato beca2(5748, 40000, 1);
    //Creando 2 instancias de la Clase BecaEstandar
    BecaEstandar beca3(9935, 70000, 0);
    BecaEstandar beca4(7548, 70000, 0);
    //Creando un vector para almacenar las becas creadas anteriormente
    vector < Beca*> becas;
    //Ocupando operacion push_back para almacenar las instancias de becas en nuestro vector becas
    becas.push_back(&beca1);
    becas.push_back(&beca2);
    becas.push_back(&beca3);
    becas.push_back(&beca4);
    //Creando una instancia de Universidad.
    Universidad universidad(becas);
    //Ocupando metodos pedidos.
    universidad.estadoUniversdad();
    cout << endl;
    cout<<"La transaccion 1: "<< universidad.transaccion(9935, 5748, 20000)<<endl;
    cout << "La transaccion 2: " << universidad.transaccion(9935,1124,5000)<<endl;
    cout << "La transaccion 3: " << universidad.transaccion(9935,7548,80000)<<endl;
    cout << "La transaccion 4: " << universidad.transaccion(5748,1124,50000)<<endl;
    cout << "La transaccion 5: " << universidad.transaccion(9935, 1124, 10000)<<endl;
    return 0;
}


