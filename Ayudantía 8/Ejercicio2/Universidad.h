#pragma once
# include < iostream >
# include < string >
# include < vector >
#include "Beca.h"
using namespace std;

class Universidad {
public:
	Universidad(vector < Beca*> b);
	bool transaccion(int id1, int id2, int d);
	void estadoUniversdad();
private:
	vector < Beca*> becas;
	int ganancias;
};

Universidad::Universidad(vector < Beca*> b) {
	for (int i = 0; i < b.size(); i++) {
		becas.push_back(b[i]);
	}
	this->ganancias = 0;
}

bool Universidad::transaccion(int id1, int id2, int d) {
	int ind1 = 0;
	int ind2 = 0;
	for (int i = 0; i < becas.size(); i++) {
		if (becas[i]->getId() == id1) {
			ind1 = i;
		}
	}
	for (int i = 0; i < becas.size(); i++) {
		if (becas[i]->getId() == id2) {
			ind2 = i;
		}
	}
	bool resultado = (this->becas[ind1])->retirar(d);
	if (resultado != false) {
		resultado = becas[ind2]->depositar(d);
		if (resultado != false) {
			return resultado;
		}
		else {
			return resultado;
		}
	}
	else {
		return resultado;
	}
}

void Universidad::estadoUniversdad() {
	cout << "Estado de universidad " << endl<<endl;
	for (int i = 0; i < becas.size(); i++) {
		cout << "Beca numero: " << i << endl;
		cout << "Tipo de beca: " << becas[i]->getTipo()<<endl;
		cout <<"Id: " << becas[i]->getId()<<endl;
		cout << "Saldo: " << becas[i]->getSaldo()<<endl;
		cout << endl;
	}
}

