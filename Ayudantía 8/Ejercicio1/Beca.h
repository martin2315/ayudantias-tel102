#pragma once

# include < iostream >
# include < string >
# include < vector >
using namespace std;

const int cobro = 1000; // Cobro por transaccion realizada
const int saldoMax_bnovato = 50000; // Saldo maximo Beca Novato
const int retiroMax_bnovato = 5000; // Retiro maximo por transaccion Beca Novato

class Beca {
	//Metodos y constructores de la clase beca.
public:
	Beca(int id, int s, int tipo);
	int getId() { return idBeca; };
	int getSaldo() { return saldo; };
	string getTipo();
	bool virtual depositar(int d) = 0; // Deposita cantidad d de dinero
	bool virtual retirar(int d) = 0; // Retira cantidad d de dinero

//atributos de la clase Beca
protected:
	int idBeca;
	int saldo;
	int tipoBeca;
};


//Metodo para imprimir que tipo de beca es nuestra instancia.
string Beca::getTipo() {
	if (this->tipoBeca == 0) {
		return "Beca novato";
	}
	else {
		return "Beca Estandar";
	}
}

//Constructor de nuestra clase beca.
Beca::Beca(int id, int s, int tipo) {
	//Inicializando atributos de la clase.
	this->idBeca = id;
	this->saldo = s;
	this->tipoBeca = tipo;
}
