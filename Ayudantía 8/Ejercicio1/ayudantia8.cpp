
#include <iostream>
#include<math.h>
#include<vector>
using namespace std;

class PoligonReg {
public:
    PoligonReg(int n, float l); //Constructor, inicializa atributos
    virtual float getArea();  //Retorna area de poligono
    virtual float getPerimetro(); //Retorna perimetro del poligono
protected:
    int n_lados;  //Numero de lados
    float lado;   //Largo de sus lados
};

PoligonReg::PoligonReg(int n, float l) {
    this->n_lados = n;
    this->lado = l;
}

float PoligonReg::getArea() {
    float apotema;
    float anguloCentral;
    float area;
    anguloCentral = 360 / n_lados;
    anguloCentral = anguloCentral * 3.14 / 180;
    apotema = lado / (2 * tan(anguloCentral / 2));
    area = n_lados * lado * apotema / 2;
    return area;
}

float PoligonReg::getPerimetro() {
    return n_lados * lado;
}

class Triangulo : public PoligonReg {
public:
    Triangulo(int n, int l) : PoligonReg(n, l) {};
    
};


class Cuadrado : public PoligonReg {
public:
    Cuadrado(int n, int l) : PoligonReg(n, l) {};
};

vector<PoligonReg> addCuadrado(vector<PoligonReg> vecp, Cuadrado c) {
    vecp.push_back(c);
    return vecp;
}


int main()
{

    int n;
    float apotema;
    float anguloCentral;
    float lado;
    float area;
    cout << "Ingrese numero de lados: ";
    cin >> n;
    cout << "Ingrese largo de lado: ";
    cin >> lado;
    PoligonReg poligono1(n, lado);
    cout << "Area: "<< poligono1.getArea()<<endl;
    cout << "Perimetro: " << poligono1.getPerimetro()<<endl;
    Triangulo triangulo1 (3, 3);
    cout << "Area: " << triangulo1.getArea() << endl;
    cout << "Perimetro: " << triangulo1.getPerimetro()<<endl;
    Cuadrado cuadrado1(4, 4);
    cout << "Area: " << cuadrado1.getArea() << endl;
    cout << "Perimetro: " << cuadrado1.getPerimetro()<<endl;
    vector<PoligonReg> vector;
    vector.push_back(poligono1);
    vector.push_back(triangulo1);
    vector = addCuadrado(vector, cuadrado1);
    cout << endl;
    cout << "-----Probando vector-----" << endl;
    cout << "Area: " << vector[2].getArea() << endl;
    cout << "Perimetro: " << vector[2].getPerimetro();
    return 0;
}

