﻿
#include <iostream>
using namespace std;
//estructura creada por nosotros
struct cod {
    char digito;
    cod* connection;
};


//Funcion que crea una secuencia de estructuras cod enlazadas.
cod* createSequence(int length) {
    cod* punteroaPrimerElemento = NULL; //puntero que apuntará al primer elemento de la secuencia
    cod* temp = NULL;  //puntero temporal que lo ocuparemos para avanzar de estructura en estructura
    char digito;   //variable donde se guardara lo ingresado por el usuario
    //for ocupado para crear 'length' estructuras
    for (int i = 0; i <length; i++) {
        if (punteroaPrimerElemento == NULL) {
            punteroaPrimerElemento = new cod;
            cout << "Ingrese el digito: " << endl;
            cin >> digito;
            //(ocupar 1° o 2°)
             punteroaPrimerElemento->digito = digito; //1° opcion (en esta linea se setea el digito ingresado por el usuario en la estructura.
            //(*punteroaPrimerElemento).digito = digito;  //2°opcion
            temp = punteroaPrimerElemento;   // hacemos que el puntero temporal que creamos apunte a la primera estructura creada.
        }
        else {
            cout << "Ingrese el digito: " << endl;
            cin >> digito;
            temp->connection = new cod;
            temp = temp->connection;
            temp->digito = digito;

        } 
    }
    temp->connection = NULL;
    return punteroaPrimerElemento;
}

void imprimirSecuencia(cod* secuencia) {
    while (secuencia != NULL) {
        cout << secuencia->digito<<" ";
        secuencia = secuencia->connection;
    }
}

int largodeSecuencia(cod* secuencia) {
    int contador = 0;
    while (secuencia != NULL) {
        contador++;
        secuencia = secuencia->connection;
    }
    return contador;
}

cod* cortarCadenaEnMitad(cod* secuencia) {
    cod* ptemp = NULL;
    int largo_secuencia = largodeSecuencia(secuencia);
    int mitad = 0;
    if (largo_secuencia % 2 == 0) {
        mitad = largo_secuencia / 2;
    }
    else {
        mitad = (largo_secuencia - 1) / 2;
    }
    for (int i = 0; i < mitad; i++) {
        ptemp = secuencia;
        secuencia = secuencia->connection;
    }
    ptemp->connection = NULL;
    return secuencia;
    
}


int main()
{

    cod* h2 = NULL;
    cod* h3 = createSequence(5);
    imprimirSecuencia(h3);
    int largo = largodeSecuencia(h3);
    cout << "largo cadena original: " << largo<<endl;
    h2 = cortarCadenaEnMitad(h3);
    cout << "nuevo largo de cadena original: " << largodeSecuencia(h3) <<endl;
    cout << "imprimiendo cadena original: ";
    imprimirSecuencia(h3);
    cout << endl;
    cout << "imprimiendo nueva cadena: ";
    imprimirSecuencia(h2);
    

 
    return 0;

}


