
#include <iostream>
#include<cstring>
using namespace std;
//Referencia, punteros y asignacion de memoria.

//Paso por valor:
void doIt_porValor(int n) 
{
    n = 10;
}

//Paso por referencia:
void doIt_porReferencia(int&n) { 
    n = 10;
}

//Retornar por valor
int larger1(int x, int y) //funcion retorna  el mayor valor entre dos numeros
{
    if (x > y){
        return x;
    }
    return y;
}

//Retornar por referencia
int& larger2(int& x, int& y)
{
    if (x > y){
        return x;
    }
    return y;
}

int main()
{
    //Ejemplo declaracion variable de referencia
    int numero = 7;
    int& numeroFavorito = numero;

    cout << "Variable original: " << numero << endl;
    cout << "Variable referencia (alias): " << numeroFavorito << endl;
    cout << "direccion de memoria de variable original: " << &numero << endl;
    cout << "direccion de memoria de variable de referencia: " << &numeroFavorito << endl;

    //Errores tipicos: no podemos redefinir una variable de referencia.
    //int numero2 = 92;
    //int& nuevoNumeroFavorito = numero2;
    //int numero3 = 80;*/
    //int& nuevoNumeroFavorito = numero2; //aqui se produce el error.


    //Forma correcta
    int numero2 = 92;
    int& nuevoNumeroFavorito = numero2;
    int numero3 = 80;
    nuevoNumeroFavorito = numero3;
    cout << "Mi nuevo numero favorito es: "<< nuevoNumeroFavorito<<endl;

    //recordar que numero es 7
    cout << "El valor original de numero es: " << numero << endl;
    doIt_porValor(numero); //no cambia el valor de numero
    cout << "El valor de numero despues de pasar por la funcion de paso por valor es: " << numero << endl;
    doIt_porReferencia(numero);
    cout << "El valor de numero despues de pasar por la funcion de paso por referencia es: " << numero << endl;

    //Retornar por referencia
    int x = 10;
    int y = 20;
    int z1 = larger1(x, y); //larger1 retorna por valor 
    int z2 = larger1(x, y); //larger2 retorna por referencia.                                                                                           ejemplo de que un amigo te dice el alias de su numero favorito y lo copias
    cout <<"El valor de retornar el numero mas largo por valor es: "<< z1<<endl;
    cout << "El valor de retornar el numero mas largo por referencia es: " << z2 << endl;
    
    return 0;
}
