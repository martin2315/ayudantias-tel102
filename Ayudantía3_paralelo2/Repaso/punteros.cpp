
#include <iostream>
using namespace std;
void swap(int* first, int* second) {
	int temp;
	temp = *second;
	*second = *first;
	*first = temp;
}

struct cod {
	int digito;
	cod* coneccion;
};

int main(){
	//estructura en memoria stack
	cod estructura1;
	estructura1.digito = 30;
	estructura1.coneccion = NULL;
	int numero = 10;
	//asignacion de valor a la variable puntero.
	int* p_numero = &numero;
	cout << "recuperacion directa de numero: " << numero << endl;
	cout << "recuperacion indirecta de numero : " << *p_numero<<endl;
	//si quisieramos cambiar el valor a "numero" ocupando la variable puntero
	*p_numero = 100000;
	cout << endl;
	cout << "Cambiando valor de variable 'numero'" << endl;
	cout << "recuperacion directa de numero: " << numero << endl;
	cout << "recuperacion indirecta de numero : " << *p_numero << endl<<endl;

	int numero2 = 2;
	//�Como podemos hacer que p_numero apunte a numero2 ?
	p_numero = &numero2;
	cout << "ahora *p_puntero apunta a la variable 'numero2' con valor : " << *p_numero<<endl;
	cout << "recuperacion directa de numero2: " << numero2 << endl;
	cout << "recuperacion indirecta de numero : " << *p_numero << endl<<endl;


	//Ocupando la funcion swamp para cambiar el valor de dos numero
	int a = 10;
	int b = 20;
	cout << "El valor de 'a' antes de entrar a la funcion: " << a << endl;
	cout << "El valor de 'b' antes de entrar a la funcion: " << b << endl;
	swap(&a, &b);
	cout << "El valor de 'a' despues de entrar a la funcion: " << a << endl;
	cout << "El valor de 'b' despues de entrar a la funcion: " << b << endl;


	//crear structuras en memoria dinamica
	cod* pcod = new cod;
	cod* pcod2 = new cod;
	//seteando valores a los elementos de la estructura
	pcod->digito = 30;
	pcod->coneccion = NULL;
	pcod2->digito = 20;
	pcod2->coneccion = NULL; 

	//conectando las estructuras;
	pcod->coneccion = pcod2;
	cout << "solicitnado el digito guardado en 'pcod2', ocupando pcod: " << (pcod->coneccion)->digito<<endl;
	delete pcod;
	delete pcod2;
	return 0;


}


