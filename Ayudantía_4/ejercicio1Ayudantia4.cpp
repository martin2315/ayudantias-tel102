
#include <iostream>
using namespace std;

void swap(int& numero1, int &numero2) {
    int temp = 0;
    temp = numero1;
    numero1 = numero2;
    numero2 = temp;
}


int main()
{

    int numero1 = 30;
    int numero2 = 40;
    cout << "Numero1: " << numero1 << " " << "Numero2: " << numero2<<endl;
    swap(numero1, numero2);
    cout << "Numero1: " << numero1 << " " << "Numero2: " << numero2 << endl;
    return 0;
}

