
#include <iostream>
using namespace std;

int main()
{

    int arreglo[5] = { 2,4,0,1,3 };
    int temp = 0;
    for (int i = 0; i <5; i++) {
        for (int j = 0; j < 5;j++) {
            if (*(arreglo+i) < *(arreglo+j)) {
                temp = *(arreglo+j);
                *(arreglo+ j) = *(arreglo + i);
                *(arreglo +i) = temp;
            }

        }
    }

    for (int i = 0; i < 5; i++) {
        cout << *(arreglo + i) << endl;
    }
    return 0;
}

