
#include <iostream>
using namespace std;

int main()
{

	float matriz1[2][2] = { {3,4},{5,6 } };
	float matriz2[2][2] = { {3,4},{5,6 } };
	float matrizResultado[2][2];

	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			(*(*(matrizResultado + i) + j)) = (*(*(matriz1 + i) + 0)) * (*(*(matriz2 + 0) + j)) + (*(*(matriz1 + i) + 1)) * (*(*(matriz2 + 1) + j));
		}
	}

	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			cout << *(*(matrizResultado + i) + j);
			cout << " ";
		}
		cout << endl;
	}
	return 0;
}

