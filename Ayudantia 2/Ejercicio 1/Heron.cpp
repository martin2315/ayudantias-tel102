#include <iostream>
#include <cmath>

using namespace std;

int main() {
  double a, b, c, s, area;
  cout<<"Ingrese el primer lado: ";
  cin>>a;
  cout<<"Ingrese el segundo lado: ";
  cin>>b;
  cout<<"Ingrese el tercer lado: ";
  cin>>c;

  s = a+b+c;
  s = s/2;
  area = s*(s-a)*(s-b)*(s-c);
  area = sqrt(area);

  cout<<"El area del triangulo ingresado es: "<<area<<endl;
  return 0;
}
