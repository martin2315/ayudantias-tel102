 #include "funciones.h"

 using namespace std;

 double capitalFinalSimple(int capitalInicial, int numeroCuotas, double interes){
   double capitalFinal = 1 + numeroCuotas * interes;
   capitalFinal *= capitalInicial;
   return capitalFinal;
 }
 double capitalFinalCompuesto(int capitalInicial, int numeroCuotas, double interes){
   double capitalFinal = 1 + interes;
   capitalFinal = pow(capitalFinal , numeroCuotas);
   capitalFinal *= capitalInicial;
   return capitalFinal;
 }

 double valorCuota(double capitalFinal, int numeroCuotas){
   return capitalFinal/numeroCuotas;
 }
