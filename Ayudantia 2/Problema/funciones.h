#include <iostream>
#include <cmath>

double capitalFinalSimple(int capitalInicial, int numeroCuotas, double interes);
double capitalFinalCompuesto(int capitalInicial, int numeroCuotas, double interes);

double valorCuota(double capitalFinal, int numeroCuotas); //Para esta parte la guia
                                                          //especifica dos funciones,
                                                          //pero es una sola necesaria
