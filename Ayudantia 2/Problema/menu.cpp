#include "funciones.h"

using namespace std;

int main(){

  cout<<"Bienvenido al sistema bancario Stonks++"<<endl;
  bool menu = true;
  int capitalInicial, numeroCuotas;
  double interes, capitalFinal, vCuota, ganancia;
  int opcion;
  while (menu) {
    cout<<endl;
    cout<<"1. Calculo Interes Simple"<<endl;
    cout<<"2. Calculo Interes Complejo"<<endl;
    cout<<"3. Calculo Interes con Mayor Ganacia"<<endl;
    cout<<"0. Salir"<<endl;
    cout<<"Ingrese una opcion: ";
    cin>>opcion;

    switch (opcion) {
      case 1:
        cout<<"Ingrese el Capital Inicial: ";
        cin>>capitalInicial;
        cout<<"Ingrese el Numero de cuotas: ";
        cin>>numeroCuotas;
        cout<<"Ingrese la tasa de Interes: ";
        cin>>interes;

        capitalFinal = capitalFinalSimple(capitalInicial, numeroCuotas, interes);
        vCuota = valorCuota(capitalFinal, numeroCuotas);
        ganancia = capitalFinal - capitalInicial;

        cout<<endl<<"El capital final es de $"<<capitalFinal<<endl;
        cout<<"El valor de la cuota es de $"<<vCuota<<endl;
        cout<<"Las ganancias son de $"<<ganancia<<endl;
        break;

      case 2:
        cout<<"Ingrese el Capital Inicial: ";
        cin>>capitalInicial;
        cout<<"Ingrese el Numero de cuotas: ";
        cin>>numeroCuotas;
        cout<<"Ingrese la tasa de Interes: ";
        cin>>interes;

        capitalFinal = capitalFinalCompuesto(capitalInicial, numeroCuotas, interes);
        vCuota = valorCuota(capitalFinal, numeroCuotas);
        ganancia = capitalFinal - capitalInicial;

        cout<<endl<<"El capital final es de $"<<capitalFinal<<endl;
        cout<<"El valor de la cuota es de $"<<vCuota<<endl;
        cout<<"Las ganancias son de $"<<ganancia<<endl;
        break;

      case 3:
        cout<<"Ingrese el Capital Inicial: ";
        cin>>capitalInicial;
        cout<<"Ingrese el Numero de cuotas: ";
        cin>>numeroCuotas;
        cout<<"Ingrese la tasa de Interes: ";
        cin>>interes;

        if(capitalFinalSimple(capitalInicial, numeroCuotas, interes)>capitalFinalCompuesto(capitalInicial, numeroCuotas, interes)){
          capitalFinal = capitalFinalSimple(capitalInicial, numeroCuotas, interes);
          vCuota = valorCuota(capitalFinal, numeroCuotas);
          ganancia = capitalFinal - capitalInicial;
          cout<<endl<<"El tipo de interes mas conveniente es el simple"<<endl;
          cout<<"El capital final es de $"<<capitalFinal<<endl;
          cout<<"El valor de la cuota es de $"<<vCuota<<endl;
          cout<<"Las ganancias son de $"<<ganancia<<endl;
        }
        else if(capitalFinalSimple(capitalInicial, numeroCuotas, interes)<capitalFinalCompuesto(capitalInicial, numeroCuotas, interes)){
          capitalFinal = capitalFinalCompuesto(capitalInicial, numeroCuotas, interes);
          vCuota = valorCuota(capitalFinal, numeroCuotas);
          ganancia = capitalFinal - capitalInicial;
          cout<<endl<<"El tipo de interes mas conveniente es el compuesto"<<endl;
          cout<<"El capital final es de $"<<capitalFinal<<endl;
          cout<<"El valor de la cuota es de $"<<vCuota<<endl;
          cout<<"Las ganancias son de $"<<ganancia<<endl;
        }
        break;

      case 0:
        cout<<"Hasta luego!"<<endl;
        menu = false;
        break;
      default:
        cout<<"Opcion no valida, intente nuevamente"<<endl;
    }
  }
  return 0;
}
