#include <iostream>

using namespace std;

int main(){

  int numero, resto;
  int inverso = 0;
  cout<<"Ingrese el numero a invertir: ";
  cin>>numero;
  while(numero!=0){
    resto = numero%10;
    inverso = inverso*10 + resto;
    numero/=10;
  }
  cout<<"El numero invertido es: "<<inverso<<endl;

  return 0;
}
