#include "Envio.h"

Envio::Envio(int Id, string direccion, bool recibido, int precio) {
	this->Id = Id;
	this->direccion = direccion;
	this->precio = precio;
	this->recibido = recibido;
}

void Envio::cambiarDireccion(string nuevaDireccion) {
	//direccion = nuevaDireccion; //Es lo mismo que la linea de abajo
	this->direccion = nuevaDireccion;
}

void Envio::marcarRecibido(bool Recibido) {
	this->recibido = Recibido;
}

void Envio::printDescripcion() {
	cout << "Id: " << this->Id<<endl;
	cout << "Direccion: " << this->direccion << endl;
	cout << "Precio: " << this->precio << endl;
	cout << "Estado: " << this->recibido << endl;
}