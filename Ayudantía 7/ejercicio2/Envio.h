#pragma once
#include<string>
#include<iostream>
using namespace std;
class Envio
{
private:
	string direccion;
	bool recibido;
public:
	int Id;
	int precio;
	Envio(int Id, string direccion, bool recibido, int precio);
	void cambiarDireccion(string nuevaDireccion);
	void marcarRecibido(bool Recibido);
	void printDescripcion();
};

