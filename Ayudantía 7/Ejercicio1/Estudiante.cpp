#include "Estudiante.h"


Estudiante::Estudiante(string Anombre, int cantidadNotas) {
	this->Nombre = Anombre;
	this->FcantidadNotas = cantidadNotas;
	this->Pnotas = new float[this->FcantidadNotas];
	//Inicializamos todas las notas en 0
	for (int i = 0; i < this->FcantidadNotas; i++) {
		this->Pnotas[i] = 0;
	}
}

Estudiante::Estudiante() {
	this->Nombre = "Estudiante";
	this->Pnotas = NULL;
}

void Estudiante::insertarNota(float nota, int posicionNota) {
	if (this->Pnotas != NULL) {
		if (0 < posicionNota <= this->FcantidadNotas) {
			this->Pnotas[posicionNota - 1] = nota;
			//Pnotas[posicionNota - 1] = nota;
			// *(Pnotas + posicionNota - 1) = nota;
		}
	}
	else {
		cout << "No existe espacio para ingresar notas: " << endl;
	}
}

float Estudiante::promedio() {
	float suma = 0;
	float promedio = 0;
	if (Pnotas != NULL) {
		for (int i = 0; i < this->FcantidadNotas; i++) {
			suma = suma + this->Pnotas[i];
		}
	}
	else {
		cout << "Estudiante no cuenta con  notas.";
		return -1;
	}
	promedio = suma / float(this->FcantidadNotas);
	return promedio;
}

Estudiante::~Estudiante() {
	if (this->Pnotas != NULL) {
		delete[] this->Pnotas;
	}
}

