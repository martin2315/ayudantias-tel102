
#include <iostream>
#include<string>
#include "Estudiante.h"
using namespace std;
int main()
{
    //punteros a instancias de clase
    Estudiante* pEstudiante1 = new Estudiante("Jaimito", 5);
    Estudiante* pEstudiante2 = new Estudiante();

    //Ingresando notas;
    pEstudiante1->insertarNota(3.2, 1);
    (*pEstudiante1).insertarNota(3.3, 2);
    pEstudiante1->insertarNota(7.0, 3);
    (*pEstudiante1).insertarNota(3.3, 4);
    pEstudiante1->insertarNota(7.0, 5);


    pEstudiante2->insertarNota(3231.0, 3);
    cout << endl;

    cout << "Nombre: " << pEstudiante1->Nombre<<endl;
    cout << "Promedio: " << pEstudiante1->promedio();
    cout << endl;
    cout << "Nombre: " << pEstudiante2->Nombre << endl;
    cout << "Promedio: " << pEstudiante2->promedio();

    pEstudiante1->~Estudiante();
    pEstudiante2->~Estudiante();
    pEstudiante1 = NULL;
    pEstudiante2 = NULL;


    return 0;
}

