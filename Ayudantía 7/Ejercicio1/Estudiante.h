#pragma once

#include<iostream>
#include<string>
using namespace std;

class Estudiante
{
private:
	float* Pnotas;
	int FcantidadNotas;
public:
	Estudiante();
	Estudiante(string ANombre, int ACantidadDeNotas);
	~Estudiante();
	void insertarNota(float nota, int posicionNota);
	float promedio();
	string Nombre;

};

